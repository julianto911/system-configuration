package configuration

import (
	"os"
	"time"

	"github.com/jinzhu/configor"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//AppConfiguration config for application
type AppConfiguration struct {
	Debug    bool
	Timezone string
	Port     string
	Location *time.Location `anonymous:"true"`
	LogPath  string
	UseDB    bool
	Name     string
}

//SystemContext  config for system
type SystemContext struct {
	App   AppConfiguration
	DB    *gorm.DB
	L     *zap.Logger
	DBCfg DBConfiguration
}

//Initiate init system configuration
func (c *SystemContext) Initiate(cfgFile string) error {
	//load configuration from file
	err := configor.New(&configor.Config{
		ErrorOnUnmatchedKeys: true,
		ENVPrefix:            "-",
		Debug:                true,
		Verbose:              true,
	}).Load(c, cfgFile)
	if err != nil {
		return errors.Wrap(err, "can't load config")
	}
	c.App.Location, err = time.LoadLocation(c.App.Timezone)
	if err != nil {
		return errors.Wrap(err, "can't load location "+c.App.Timezone)
	}

	//init logger
	if err := c.initLog(); err != nil {
		return err
	}

	if c.App.UseDB {
		db, err := c.DBCfg.Init(c.L)
		if err != nil {
			return err
		}
		c.DB = db
	}

	return nil
}

//InitLog initiate log configuration
func (c *SystemContext) initLog() error {
	file, err := os.Create(c.App.LogPath + c.App.Name + "_" + time.Now().Format("2006_01_02__15_04") + ".log")
	if err != nil {
		return errors.Wrap(err, "can't create log file")
	}

	//configure logging
	if c.App.Debug {
		c.L = configureLog(true, file)
	} else {
		c.L = configureLog(false, file)
	}
	return nil
}

func configureLog(d bool, f *os.File) *zap.Logger {

	pe := zap.NewProductionEncoderConfig()

	fileEncoder := zapcore.NewJSONEncoder(pe)
	pe.EncodeTime = zapcore.ISO8601TimeEncoder
	consoleEncoder := zapcore.NewConsoleEncoder(pe)

	level := zap.InfoLevel
	if d {
		level = zap.DebugLevel
	}

	core := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, zapcore.AddSync(f), level),
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), level),
	)

	l := zap.New(core)

	return l
}
