package configuration

import (
	"fmt"

	"github.com/hypnoglow/gormzap"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" //mysql
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

// DBConfiguration is config for database
type DBConfiguration struct {
	Host            string `required:"true"`
	Port            string `required:"true"`
	Name            string `required:"true"`
	User            string `required:"true"`
	Password        string `required:"true"`
	Type            string `required:"true"`
	Schema          string
	ApplicationName string
	ConnectTimeout  int  `default:"30"`
	Logging         bool `default:"false"`
	MaxOpenConn     int  `default:"50"`
	MaxIdleConn     int  `default:"10"`
	Prefix          string
}

//Init iniatiate database connection
func (c *DBConfiguration) Init(l *zap.Logger) (*gorm.DB, error) {
	var db *gorm.DB
	var err error

	if c.Type == "mysql" {
		l.Debug("Connect to mysql")
		db, err = gorm.Open("mysql", c.makeMySQLString())
	} else {
		l.Debug("Connect to postgresql")
		if c.Schema == "" {
			db, err = gorm.Open("postgres", c.makePostgresString())
		} else {
			db, err = gorm.Open("postgres", c.makePostgresWithSchemaString())
		}
	}
	if err != nil {
		return nil, errors.Wrap(err, "can't open db connection")
	}

	// Set log mode
	db.LogMode(c.Logging)

	// Log via zap
	//db.SetLogger(gormzap.New(l, gormzap.WithLevel(zap.InfoLevel)))
	db.SetLogger(gormzap.New(l))

	// Set max pool
	db.DB().SetMaxIdleConns(c.MaxIdleConn)
	db.DB().SetMaxOpenConns(c.MaxOpenConn)

	return db, err
}

func (c *DBConfiguration) makeMySQLString() string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True",
		c.User, c.Password, c.Host, c.Name)
}

func (c *DBConfiguration) makePostgresString() string {
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s connect_timeout=%d application_name=%s",
		c.Host, c.Port, c.User, c.Name, c.Password, c.ConnectTimeout, c.ApplicationName)
}

func (c *DBConfiguration) makePostgresWithSchemaString() string {
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s connect_timeout=%d application_name=%s search_path=%s",
		c.Host, c.Port, c.User, c.Name, c.Password, c.ConnectTimeout, c.ApplicationName, c.Schema)
}
